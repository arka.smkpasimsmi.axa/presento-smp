<div class="row">
        <div class="col-md-3">
          <?php if ($this->uri->segment(2)=="tulisPesan"): ?>
            <a href="<?= base_url('Feedback/listPesan') ?>" class="btn tema-biru text-white btn-block mb-3">Kembali ke Pesan Masuk</a>
          <?php else: ?>
            <a href="<?= base_url('Feedback/tulisPesan') ?>" class="btn tema-biru text-white btn-block mb-3">Tulis Pesan</a>
          <?php endif ?>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Folders</h3>
            </div>
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item <?= $this->uri->segment(2) == 'listPesan' ? 'sub-tema-biru' : '' ?>">
                  <a href="<?= base_url('Feedback/listPesan') ?>" class="nav-link text-white <?= $this->uri->segment(2) == 'listPesan' ? 'text-white' : 'text-dark' ?>">
                    <i class="fas fa-inbox <?= $this->uri->segment(2) == 'listPesan' ? 'text-white' : 'text-dark' ?>"></i>  Pesan Masuk
                    <!-- <span class="badge bg-primary float-right">12</span> -->
                  </a>
                </li>
                <li class="nav-item <?= $this->uri->segment(2) == 'terkirimPesan' ? 'sub-tema-dark' : '' ?>">
                  <a href="<?= base_url('Feedback/terkirimPesan') ?>" class="nav-link <?= $this->uri->segment(2) == 'terkirimPesan' ? 'text-white' : 'text-dark' ?>">
                    <i class="far fa-envelope <?= $this->uri->segment(2) == 'terkirimPesan' ? 'text-white' : 'text-dark' ?>"></i> Terkirim
                  </a>
                </li>
                <li class="nav-item <?= $this->uri->segment(2) == 'pentingPesan' ? 'sub-tema-dark' : '' ?>">
                  <a href="<?= base_url('Feedback/pentingPesan') ?>" class="nav-link <?= $this->uri->segment(2) == 'pentingPesan' ? 'text-white' : 'text-dark' ?>">
                    <i class="far fa-star <?= $this->uri->segment(2) == 'pentingPesan' ? 'text-white' : 'text-dark' ?>"></i> Penting
                  </a>
                </li>
                <li class="nav-item <?= $this->uri->segment(2) == 'sampahPesan' ? 'sub-tema-dark' : '' ?>">
                  <a href="<?= base_url('Feedback/sampahPesan') ?>" class="nav-link <?= $this->uri->segment(2) == 'sampahPesan' ? 'text-white' : 'text-dark' ?>">
                    <i class="far fa-trash-alt <?= $this->uri->segment(2) == 'sampahPesan' ? 'text-white' : 'text-dark' ?>"></i> Sampah 
                  </a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>