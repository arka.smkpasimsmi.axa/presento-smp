<?php $beranda = $this->crud->get('tb_m_beranda') ?>  <div class="app-sidebar" style="background-color: #343f53;">
        <div class="sidebar-header" style="background-color: #343f53;">
            <a class="header-brand" href="<?php base_url('Admin') ?>">
                <div class="logo-img">
        <?php foreach ($beranda as $data) : ?>
                   <img src="<?= base_url('assets/images/beranda_images/'.$data->ikon) ?>" class="header-brand-img" height="30" width="30" alt="logo"> 
        <?php endforeach; ?>
                </div>
                <span class="text" style="color: #fff;">Sisfo</span>
            </a>
            <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
            <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
        </div>
        
            
        <div class="sidebar-content">
            <div class="nav-container">
                <nav id="main-menu-navigation" class="navigation-main">
                    <div class="nav-lavel" style="background-color: #343f53;"></div>
                    <?php if ($this->session->userdata('role') == 'guru'): ?>
                    <div class="nav-item tema-dark <?= $this->uri->segment(1) == 'Admin_guru' ? 'active' : '' ?>">
                        <a href="<?= base_url('Admin_guru/profileGuruSingle') ?>"><i class="ik ik-user text-light"></i><span class="text-light">Profil</span></a>
                    </div>
                    <?php elseif($this->session->userdata('role') == 'admin'): ?>
                        <div class="nav-item tema-dark <?= $this->uri->segment(1) == 'Admin' ? 'active' : '' ?>">
                        <a href="<?= base_url('Admin') ?>"><i class="ik ik-bar-chart-2 text-light"></i><span class="text-light">Dashboard</span></a>
                    </div>
                    <div class="nav-item tema-dark <?= $this->uri->segment(1) == 'Admin_ppdb' ? 'active' : '' ?>">
                        <a href="<?= base_url('Admin_ppdb') ?>"><i class="ik ik-clipboard text-light"></i><span class="text-light">PPDB</span> <span class="badge badge-success">New</span></a>
                    </div>
                    <div class="nav-item has-sub tema-dark <?= $this->uri->segment(1) == 'Admin_guru' ? 'open active' : '' ?>">
                        <a style="color:white;" href="javascript:void(0)"><i class="ik ik-users text-light"></i><span class="text-light">Pengguna</span> <span class="badge badge-danger">150+</span></a>
                        <div class="submenu-content">
                            <a href="<?= base_url('Admin_guru') ?>" class="menu-item sub-tema-biru <?= $this->uri->segment(1) == 'Admin_guru' ? 'text-default' : 'text-light' ?>">Guru</a>
                        </div>
                    </div>
                    <div class="nav-item tema-dark <?= $this->uri->segment(1) == 'Feedback' ? 'active' : '' ?>">
                        <a href="<?= base_url('Feedback/listPesan') ?>"><i class="ik ik-mail text-light"></i><span class="text-light">Feedback</span> <span class="badge badge-success">New</span></a>
                    </div>
                    <div class="nav-lavel text-light" style="background-color: #343f53;">Front-End</div>
                    <div class="nav-item tema-dark <?= $this->uri->segment(1) == 'Admin_beranda' ? 'active' : '' ?>">
                        <a href="<?= base_url('Admin_beranda') ?>"><i class="ik ik-home text-light"></i><span class="text-light">Beranda</span></a>
                    </div>
                    <div class="nav-item has-sub tema-dark <?= $this->uri->segment(1) == 'Profil_sekolah' ? 'open active' : '' ?>">
                        <a style="color:white;" href="#"><i class="ik ik-box text-light"></i><span class="text-light">Profil</span></a>
                        <div class="submenu-content">
                            <a href="<?= base_url('Profil_sekolah/indexStruktur') ?>" class="menu-item sub-tema-biru text-light <?= $this->uri->segment(2) == 'indexStruktur' ? 'text-default' : 'text-light' ?>">Struktur Organisasi</a>
                            <a href="<?= base_url('Profil_sekolah/listKelas') ?>" class="menu-item sub-tema-biru <?= $this->uri->segment(2) == 'list' || $this->uri->segment(2) == 'detail' ? 'text-default' : 'text-light' ?>">List Kelas</a>
                        </div>
                    </div>
                    <div class="nav-item has-sub tema-dark <?= $this->uri->segment(1) == 'Galeri_sekolah' ? 'open active' : '' ?>">
                        <a style="color:white;" href="#"><i class="ik ik-image text-light"></i><span class="text-light">Galeri</span> <span class="badge badge-success">New</span></a>
                        <div class="submenu-content">
                            <a href="<?= base_url('Galeri_sekolah/listFoto') ?>" class="menu-item sub-tema-biru text-light <?= $this->uri->segment(2) == 'listFoto' ||  $this->uri->segment(2) == 'detailFoto' ? 'text-default' : 'text-light' ?>">Foto</a>
                            <a href="<?= base_url('Galeri_sekolah/listVideo') ?>" class="menu-item sub-tema-biru text-light <?= $this->uri->segment(2) == 'listVideo' || $this->uri->segment(2) == 'detailVideo' ? 'text-default' : 'text-light' ?>">Video</a>
                        </div>
                    </div>
                    <div class="nav-item tema-dark <?= $this->uri->segment(2) == 'listEskul' || $this->uri->segment(2) == 'detailEskul' ? 'active' : '' ?>">
                        <a href="<?= base_url('Admin_eskul/listEskul') ?>"><i class="ik ik-award text-light"></i><span class="text-light">Ekstrakulikuler</span></a>
                    </div>
                    <div class="nav-item tema-dark <?= $this->uri->segment(2) == 'listPrestasi' || $this->uri->segment(2) == 'detailPrestasi' ? 'active' : '' ?>">
                        <a href="<?= base_url('Admin_prestasi/listPrestasi') ?>"><i class="ik ik-award text-light"></i><span class="text-light">Prestasi</span></a>
                    </div>
                    <div class="nav-item tema-dark <?= $this->uri->segment(2) == 'Info_sekolah' ? 'active' : '' ?>">
                        <a href="<?= base_url('Info_sekolah') ?>"><i class="ik ik-info text-light"></i><span class="text-light">Informasi Sekolah</span></a>
                    </div>
                    <?php endif ?>
   </nav>
   </div>
 </div>
</div>

    <div class="main-content">
        <div class="container-fluid">
               