<?php 
    if($this->session->userdata('role') !== 'admin')  {
    Redirect('Page/pageNotFound');
    } 
?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user" style="background-color: #6692e9;"></i>
                <div class="d-inline">
                    <h5>Struktur Sekolah</h5>
                    <span>Kontrol Struktur Sekolah disini</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('Admin') ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Profil</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Struktur Organisasi</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<?php foreach ($struktur as $d): ?>
<div class="card">
    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active show" id="pills-deskripsi-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-deskripsi" aria-selected="false">Deskripsi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-deskripsi-tab">
            <div class="card-body">
                <div class="row">
                    <h2 class="ml-3">Struktur Organisasi</h2>
                </div>
                <h6 class="text-secondary ml-1"><?= $d->deskripsi ?></h6>
                <div class="border-bottom mb-2">
                </div>
                <a href="<?= base_url('assets/images/struktur_images/'.$d->foto) ?>" class="single-popup-photo"><img src="<?= base_url('assets/images/struktur_images/'.$d->foto) ?>" class="img-fluid rounded"></a>
            </div>
        </div>
        <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
            <div class="card-body">
                <form method="POST" action="<?= base_url('Profil_sekolah/postEditStruktur/'.$d->id) ?>" id="form-edit" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleTextarea1">Deskripsi</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="7" name="deskripsi"><?= $d->deskripsi ?></textarea>
                </div>
                <div class="form-group mt-2">
                    <label>Foto</label>
                    <div class="input-group col-xs-12">
                        <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto" >
                    </div>
                    <small>(Biarkan kosong jika tidak ingin diubah)</small>
                </div>
                <input type="hidden" name="foto_lama" value="<?= $d->foto ?>">
                <a href="<?= base_url('assets/images/struktur_images/'.$d->foto) ?>" class="single-popup-photo"><img src="<?= base_url('assets/images/struktur_images/'.$d->foto) ?>" class="img-fluid rounded"></a>
                    <button class="btn btn-success mt-2" type="submit" id="btn-edit-submit">Update Data</button>
                </form>
            </div>
        </div>
    </div>
</div>
    
<?php endforeach ?>