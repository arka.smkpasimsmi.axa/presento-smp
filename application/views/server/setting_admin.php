<?php foreach ($admin as $d): ?>
    <?php if ($this->session->userdata('id') == $d->id): ?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text bg-blue"></i>
                <div class="d-inline">
                    <h5>Setting Akun Admin</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pills-setting-tab" data-toggle="pill" href="#akun" role="tab" aria-controls="pills-setting" aria-selected="false">Settings</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="akun" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <h3>Ganti Email</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 90px;">
                                            <li><i class="ik minimize-card ik-plus"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body feeds-widget" style="display: none;">
                                    <form class="form-horizontal" method="POST" action="<?= base_url('Admin/gantiEmail/'.$d->id) ?>">
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label for="example-password">Email Lama</label>
                                            <input type="email" value="<?= $d->username ?>" class="form-control" name="email_lama" id="example-password" placeholder="Email Lama" readonly>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label for="example-password">Email Baru</label>
                                            <input type="email" class="form-control" name="email_baru" id="example-password" placeholder="Email Baru">
                                            <small class="text-danger"><i><?= form_error('email_baru') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <button class="btn btn-success" type="submit">Update Email</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3>Ganti Password</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 90px;">
                                            <li><i class="ik minimize-card ik-plus"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body feeds-widget" style="display: none;">
                                    <form class="form-horizontal" method="POST" action="<?= base_url('Admin/gantiPassword/'.$d->id) ?>" >
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label>Password Lama</label>
                                            <input type="password" class="form-control" name="password_lama" placeholder="Password Lama">
                                            <small class="text-danger"><i><?= form_error('password_lama') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label>Password Baru</label>
                                            <input type="password" class="form-control" name="password_baru" placeholder="Password Baru">
                                            <small class="text-danger"><i><?= form_error('password_baru') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <button class="btn btn-success" type="submit">Update Password</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
<?php endforeach ?>