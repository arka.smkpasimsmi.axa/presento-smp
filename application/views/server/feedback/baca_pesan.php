  <div class="col-md-9">
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">Baca Pesan</h3>
      </div>
      <!-- /.card-header -->
      <?php foreach ($pesan as $d): 
        if ($d->status == "terkirim") :
        ?>
        <form method="POST" action="<?= base_url('Feedback/buangPesan/'.$d->id) ?>" id="form-delete" >
          <input type="hidden" value="list" name="menu">
        </form>
      <div class="card-body p-0">
        <div class="ml-4 mt-2">
          <h5 class="font-weight-bold"><?= $d->subject ?></h5>
          <h6>Untuk: <?= $d->email ?>
            <span class="mailbox-read-time float-right mr-4"><?= $d->created_dt ?></span></h6>
        </div>
        <div class="border mt-1"></div>
        <!-- /.mailbox-controls -->
        <div class="ml-4 mr-4 mt-2">
          <?= $d->deskripsi ?>
        </div>
        <!-- /.mailbox-read-message -->
      </div> 
      <?php else: ?>
        <div class="card-body p-0">
        <div class="ml-4 mt-2">
          <h5 class="font-weight-bold"><?= $d->subject ?></h5>
          <?php if ($d->tipe_pesan == 'penerima'): ?>
            <h6>Dari: <?= $d->nama.' | '.$d->email ?>
            <?php elseif($d->tipe_pesan == 'pengirim'): ?>
              <h6>Untuk: <?= $d->nama.' | '.$d->email ?>
          <?php endif ?>
            <span class="mailbox-read-time float-right mr-4"><?= $d->created_dt ?></span></h6>
        </div>
        <div class="border mt-1"></div>
        <!-- /.mailbox-controls -->
        <div class="ml-4 mr-4 mt-2">
          <?= $d->deskripsi ?>
        </div>
        <!-- /.mailbox-read-message -->
      </div>
      <?php 
      endif;
      endforeach; ?>
      <!-- /.card-body -->
      <div class="card-footer">
        <div class="float-right">
          
        </div>
        <?php $email = explode('@', $d->email) ?>
        <a href="<?= base_url('Feedback/tulisPesan/'.$email[0]) ?>"><button type="button" class="btn btn-default" ><i class="fas fa-share"></i> Balas </button></a>
        <?php if ($d->sampah == "false"): ?>
          <?php if ($d->favorit == "true"): ?>
            <button type="button" class="btn btn-default"><i class="fas fa-star text-warning"></i><a href="<?= base_url('Feedback/pesanFavorit/'.$d->id.'/detail/unfavorit') ?>">Penting</a></button>
            <?php elseif($d->favorit == "false"): ?>
              <button type="button" class="btn btn-default"><i class="fas fa-star "></i><a href="<?= base_url('Feedback/pesanFavorit/'.$d->id.'/detail/favorit') ?>">Penting</a></button>
          <?php endif ?>
        <?php endif ?>
        <?php if ($d->sampah == "true"): ?>
          <a href="<?= base_url('Feedback/deletePesanDetail/'.$d->id) ?>"><button type="button" class="btn btn-default" ><i class="far fa-trash-alt"></i> Hapus </button></a>
          <a href="<?= base_url('Feedback/restorePesan/'.$d->id) ?>"><button type="button" class="btn btn-default" ><i class="ik ik-rotate-ccw"></i> Restore </button></a>
          <?php else: ?>
        <a href="<?= base_url('Feedback/buangPesan/'.$d->id.'/detail') ?>"><button type="button" class="btn btn-default" ><i class="far fa-trash-alt"></i> Hapus </button></a>
        <?php endif ?>
      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>