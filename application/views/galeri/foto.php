 
 <!-- ======= foto Section ======= -->
 <section id="team" class="team">
    <div class="container mt-5" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
		<div class="section-title">
			<h2>Galeri Foto Kegiatan Sekolah</h2>
			<p>Berikut merupakan galeri kegiatan dari <?= $data->nama_sekolah; ?>.</p>
		</div>
		<?php endforeach; ?>
		
		<div class="row">
	<?php foreach ($foto as $data) : ?>	
	
        <div class="col-lg-4 col-md-4 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
				<a href="<?= base_url('assets/images/galeri_images/'.$data->item); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->item; ?>">
              		<img style="width:100%;max-height: 230px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/galeri_images/'.$data->item); ?>" class="img-fluid" alt="">
				</a>
			</div>
            <div class="member-info">
              <p><?= $data->deskripsi; ?></p>
            </div>
          </div>
		</div>
		
	<?php endforeach; ?>

      </div>

    </div>
  </section><!-- End foto Section -->