<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pesan extends CI_Model {

	public function getPesan() {
		$this->db->order_by('created_dt', 'DESC');
		return $this->db->get('tb_m_pesan')->result();
	}

}    