<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function index()
	{
		$title['title'] = 'Dashboard';
		$data=[
				'countGuru' 	=> $this->crud->countAll('tb_m_guru'),
				'countPPDB' 	=> $this->crud->countAll('tb_m_ppdb'),
				'countFeedback' => $this->crud->countAll('tb_m_pesan'),
				'countPrestasi' => $this->crud->countAll('tb_m_prestasi'),
				'countFoto'		=> $this->m_galeri->countFoto(),
				'countVideo'	=> $this->m_galeri->countVideo(),
		];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/dashboard',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function settingAdmin()
	{
		$title['title'] = 'Setting Akun Admin';
		$data=[
			'admin'		=> $this->crud->get('auth')
		];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/setting_admin',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function gantiEmail($ids)
	{
			$this->form_validation->set_rules('email_baru', 'Email', 'required|valid_email|is_unique[auth.username]', 
				['required' => 'email harus diisi',
				'valid_email' => 'masukkan email yang benar',
				'is_unique' => 'email sudah terdaftar']);

			if ($this->form_validation->run() == false) {
				$title['title'] = 'Setting Akun Admin';
				$data=[
					'admin'		=> $this->crud->get('auth')
				];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/setting_admin',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$id 		= ['id' => $ids];
				$emailBaru = $this->input->post('email_baru');
				$data = [
					'username' => $emailBaru
				];
				$this->crud->edit($id, $data, 'auth');
				$this->session->set_flashdata('success', 'Email berhasil diganti!');
				Redirect('Admin/settingAdmin');
			}
	}

	public function gantiPassword($ids)
	{
		$this->form_validation->set_rules('password_lama', 'Retype Password', 'required',
											['required' => 'password lama harus diisi!']);
		$this->form_validation->set_rules('password_baru', 'Password', 'required', 
											['required' => 'password baru harus diisi']);

		if ($this->form_validation->run() == false) {
				$title['title'] = 'Setting Akun Admin';
				$data=[
					'admin'		=> $this->crud->get('auth')
				];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/setting_admin',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$id = ['id' => $ids];
				$auth = $this->db->get_where('auth', $id)->row_array();
				$passwordLama = $this->input->post('password_lama');
				$passwordBaru = $this->input->post('password_baru');
				if (password_verify($passwordLama, $auth['password'])) {
					$data = [
						'password' => password_hash($passwordBaru, PASSWORD_DEFAULT) 
					];
					var_dump($data);
					$this->crud->edit($id, $data, 'auth');
					$this->session->set_flashdata('success', 'Password berhasil diganti!');
					Redirect('Admin/settingAdmin');
				}else{
					$this->session->set_flashdata('fail', 'Password lama tidak sesuai!');
					Redirect('Admin/settingAdmin');
				}
			}
	}
}
