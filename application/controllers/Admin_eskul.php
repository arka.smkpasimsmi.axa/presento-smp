<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_eskul extends CI_Controller {

	public function listEskul()
	{
		$title['title'] = 'List Ekstrakulikuler';
		$data = [
			'eskul'	=> $this->crud->get('tb_m_eskul')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/eskul/list_eskul',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function detailEskul($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_eskul',$id)->row_array();
		$title['title'] = 'Detail Eskul | '.$nama['judul'];
		$data = [
			'eskul'	=> $this->crud->getById('tb_m_eskul',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/eskul/detail_eskul.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function deleteEskul($id)
	{
		$this->crud->delete($id,'tb_m_eskul');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Admin_eskul/listEskul');
	}

	public function insertEskul()
	{
		$this->form_validation->set_rules('nama_eskul','Ekstrakulikuler', 'required',
    			['required' => 'Judul harus diisi!']);
			$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
	    		['required' => 'Deskripsi harus diisi!']);

			if ($this->form_validation->run()== false) {
				$title['title'] = 'List Eskul';
				$data = [
					'eskul'	=> $this->crud->get('tb_m_eskul')
					];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/front_end/eskul/list_eskul',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$nama_eskul			= $this->input->post('nama_eskul');
				$deskripsi			= $this->input->post('deskripsi');

					$config['upload_path']		= './assets/images/eskul_images/';
					$config['allowed_types']	= 'jpg|png|jpeg';
					$config['encrypt_name']		=  TRUE;
					$this->load->library('upload', $config);

					if(!$this->upload->do_upload('foto')){
						$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
						Redirect('Admin_eskul/listEskul');
					}else{
						$foto 	  = $this->upload->data('file_name');
						$data = [
							'judul'				=> $nama_eskul,
							'item'				=> $foto,
							'deskripsi'			=> $deskripsi,
							'created_by'		=> 'ADMIN'
						];
						$this->crud->insert($data,'tb_m_eskul');
						$this->session->set_flashdata('success' , 'Pendaftaran Ekstrakulikuler Berhasil!');
						Redirect('Admin_eskul/listEskul');
				}
			}
	}



	public function postEditEskul($ids) {
		$id 				= ['id' => $ids];
		$judul				= $this->input->post('judul');
		$deskripsi 			= $this->input->post('deskripsi');
		$foto 				= $this->input->post('foto');
		$foto_lama 			= $this->input->post('foto_lama');

		if ($foto !== '') {
			$config['upload_path']		= './assets/images/eskul_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $judul.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}

			$data = [
				'item'				=> $foto,
				'judul'				=> $judul,
				'deskripsi'			=> $deskripsi
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'eskul_images');
			}
			$this->crud->edit($id,$data,'tb_m_eskul');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Admin_eskul/detailEskul/').$ids);	
		
	}
}